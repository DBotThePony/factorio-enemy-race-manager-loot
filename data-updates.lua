
-- Copyright (C) 2022 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local function starts_with(self, value)
	return self == value or self:sub(1, #value) == value
end

local function lerp(t, a, b)
	if t >= 1 then return b end
	if t <= 0 then return a end
	return a + (b - a) * t
end

local function lerp_hp(hp, a, b, a1, b1)
	return lerp((hp - a) / (b - a), a1, b1)
end

local function round(value)
	return math.floor(value + 0.5)
end

if mods['SchallAlienLoot'] then
	local ore_1_chance = {
		{points = 0, chance = 0, min_amount = 0, max_amount = 1},
		{points = 200, chance = 0.7, min_amount = 0, max_amount = 2},
		{points = 500, chance = 0.8, min_amount = 0, max_amount = 3},
		{points = 1000, chance = 0.85, min_amount = 0, max_amount = 3},
		{points = 2000, chance = 1, min_amount = 1, max_amount = 4},
		{points = 6000, chance = 0, min_amount = 1, max_amount = 7},
	}

	local ore_2_chance = {
		{points = 300, chance = 0.2, min_amount = 0, max_amount = 1},
		{points = 800, chance = 0.6, min_amount = 0, max_amount = 1},
		{points = 1800, chance = 0.85, min_amount = 0, max_amount = 2},
		{points = 3000, chance = 0.95, min_amount = 1, max_amount = 4},
		{points = 6000, chance = 1, min_amount = 1, max_amount = 5},
		{points = 14000, chance = 0, min_amount = 1, max_amount = 8},
	}

	local ore_3_chance = {
		{points = 300, chance = 0.2, min_amount = 0, max_amount = 1},
		{points = 1200, chance = 0.5, min_amount = 0, max_amount = 2},
		{points = 2500, chance = 0.85, min_amount = 0, max_amount = 3},
		{points = 4000, chance = 0.95, min_amount = 0, max_amount = 4},
		{points = 9500, chance = 1, min_amount = 1, max_amount = 5},
		{points = 20000, chance = 1, min_amount = 2, max_amount = 7},
		{points = 40000, chance = 0, min_amount = 2, max_amount = 7},
	}

	local artifact_chance = {
		{points = 4000, chance = 0.2, min_amount = 0, max_amount = 2},
		{points = 8000, chance = 0.4, min_amount = 0, max_amount = 3},
		{points = 20000, chance = 0.75, min_amount = 1, max_amount = 4},
		{points = 40000, chance = 0.9, min_amount = 2, max_amount = 6},
		{points = 60000, chance = 1, min_amount = 2, max_amount = 8},
		{points = 90000, chance = 1, min_amount = 4, max_amount = 9},
		{points = 120000, chance = 1, min_amount = 7, max_amount = 15},
		{points = 150000, chance = 1, min_amount = 10, max_amount = 20},
		{points = 200000, chance = 1, min_amount = 10, max_amount = 30},
		{points = 250000, chance = 1, min_amount = 14, max_amount = 50},
		{points = 300000, chance = 1, min_amount = 18, max_amount = 80},
	}

	local resistances_strength = {
		acid = 0.1,
		cold = 0.1,
		explosion = 0.35,
		fire = 0.3,
		['kr-explosion'] = 0.35,
		laser = 0.5,
		physical = 0.6,
		poison = 0.25,
		radioactive = 0.3,
	}

	local drops = {
		{'alien-ore-1', ore_1_chance},
		{'alien-ore-2', ore_2_chance},
		{'alien-ore-3', ore_3_chance},
		{'alien-artifact', artifact_chance},
	}

	local mod_names = {'erm_vanilla'}

	if mods['erm_toss'] then
		table.insert(mod_names, 'erm_toss')
	end

	if mods['erm_zerg'] then
		table.insert(mod_names, 'erm_zerg')
	end

	local function add_drops(proto, points, no_decrease)
		local localised_description = proto.localised_description or {}

		for _, drop_info in ipairs(drops) do
			for loot_data = 1, #drop_info[2] do
				local _loot_data = drop_info[2][loot_data]

				if _loot_data.points == points or _loot_data.points < points and loot_data == #drop_info[2] then
					if _loot_data.chance > 0 then
						proto.loot = proto.loot or {}

						table.insert(proto.loot, {
							item = drop_info[1],
							probability = _loot_data.chance,
							count_min = _loot_data.min_amount,
							count_max = _loot_data.max_amount,
						})

						table.insert(localised_description, {
							'unit-description.erm-loot',
							_loot_data.min_amount,
							_loot_data.max_amount,
							data.raw.item[drop_info[1]].localised_name or {'item-name.' .. drop_info[1]},
							round(_loot_data.chance * 100)
						})

						table.insert(localised_description, '\n')
					end

					break
				elseif _loot_data.points > points or no_decrease and loot_data < #drop_info[2] and _loot_data.chance > drop_info[2][loot_data + 1].chance then
					if loot_data > 1 then
						local previous = drop_info[2][loot_data - 1]

						local new_data = {
							item = drop_info[1],
							probability = lerp_hp(points, previous.points, _loot_data.points, previous.chance, _loot_data.chance),
							count_min = round(lerp_hp(points, previous.points, _loot_data.points, previous.min_amount, _loot_data.min_amount)),
							count_max = math.max(round(lerp_hp(points, previous.points, _loot_data.points, previous.max_amount, _loot_data.max_amount)), 1),
						}

						if new_data.probability > 0 then
							proto.loot = proto.loot or {}

							table.insert(proto.loot, new_data)

							table.insert(localised_description, {
								'unit-description.erm-loot',
								new_data.count_min,
								new_data.count_max,
								data.raw.item[drop_info[1]].localised_name or {'item-name.' .. drop_info[1]},
								round(new_data.probability * 100)
							})

							table.insert(localised_description, '\n')
						end
					end

					break
				end
			end
		end

		if type(localised_description[1]) ~= '' and #localised_description ~= 0 then
			for i = #localised_description + 1, 2, -1 do
				localised_description[i] = localised_description[i - 1]
			end

			localised_description[1] = ''
		elseif #localised_description == 0 then
			localised_description = nil
		end

		proto.localised_description = localised_description
	end

	for name, proto in pairs(data.raw.unit) do
		local hit = false

		for _, MOD_NAME in ipairs(mod_names) do
			if starts_with(name, MOD_NAME) then
				hit = true
				break
			end
		end

		if hit and proto.max_health then
			local points = proto.max_health

			if proto.resistances then
				for resistance_name, data in pairs(proto.resistances) do
					local strength = resistances_strength[resistance_name] or 0.05
					local percent = data.percent or 0

					if percent >= 0.875 then
						percent = 0.875
					end

					points = points + proto.max_health * strength * (1 / (1 - percent)) + (data.decrease or 0) * strength * 8
				end
			end

			log(name .. ' has ' .. points .. ' loot points!')
			add_drops(proto, points * settings.startup['enemy-fixes-enemy-race-manager-schall-loot-scale'].value)
		end
	end

	for name, proto in pairs(data.raw.turret) do
		local hit = false

		for _, MOD_NAME in ipairs(mod_names) do
			if starts_with(name, MOD_NAME) then
				hit = true
				break
			end
		end

		if hit and proto.max_health then
			local points = proto.max_health

			if proto.resistances then
				for resistance_name, data in pairs(proto.resistances) do
					local strength = resistances_strength[resistance_name] or 0.05
					local percent = data.percent or 0

					if percent >= 0.875 then
						percent = 0.875
					end

					points = points + proto.max_health * strength * (1 / (1 - percent)) + (data.decrease or 0) * strength * 8
				end
			end

			log(name .. ' has ' .. points .. ' loot points!')
			add_drops(proto, points * settings.startup['enemy-fixes-enemy-race-manager-schall-loot-scale-turret'].value, true)
		end
	end

	for name, proto in pairs(data.raw['unit-spawner']) do
		local hit = false

		for _, MOD_NAME in ipairs(mod_names) do
			if starts_with(name, MOD_NAME) then
				hit = true
				break
			end
		end

		if hit and proto.max_health then
			local points = proto.max_health

			if proto.resistances then
				for resistance_name, data in pairs(proto.resistances) do
					local strength = resistances_strength[resistance_name] or 0.05
					local percent = data.percent or 0

					if percent >= 0.875 then
						percent = 0.875
					end

					points = points + proto.max_health * strength * (1 / (1 - percent)) + (data.decrease or 0) * strength * 8
				end
			end

			log(name .. ' has ' .. points .. ' loot points!')
			add_drops(proto, points * settings.startup['enemy-fixes-enemy-race-manager-schall-loot-scale-spawner'].value, true)
		end
	end
end
