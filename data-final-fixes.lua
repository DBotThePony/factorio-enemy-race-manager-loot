
-- Copyright (C) 2022 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local function install_loot_table(original, target, scale)
	for _, loot in ipairs(original) do
		if not mods['SchallAlienLoot'] or loot.item ~= 'alien-ore-1' and loot.item ~= 'alien-ore-2' and loot.item ~= 'alien-ore-3' and loot.item ~= 'alien-artifact' then
			table.insert(target, {
				item = loot.item,
				probability = math.max(0, math.min(1, loot.probability * math.sqrt(scale))),
				count_min = loot.count_min * scale,
				count_max = loot.count_max * scale,
			})
		end
	end
end

local function starts_with(self, value)
	return self == value or self:sub(1, #value) == value
end

local function insert_dedup(target, value)
	for _, value2 in ipairs(target) do
		if value2 == value then return end
	end

	target[#target + 1] = value
end

local ErmConfig = require('__enemyracemanager__/lib/global_config')
local MOD_NAME = 'erm_vanilla'
local biters = {}
local slashed = MOD_NAME .. '/'

for name in pairs(data.raw.unit) do
	if starts_with(name, MOD_NAME) then
		local protoName = name:sub(#slashed + 1)
		protoName = protoName:sub(1, protoName:find('/') - 1)

		insert_dedup(biters, protoName)
	end
end

for _, biterName in ipairs(biters) do
	local original = data.raw.unit[biterName]

	if original and original.loot then
		for level = 1, ErmConfig.MAX_LEVELS do
			local new = data.raw.unit[MOD_NAME .. '/' .. biterName .. '/' .. level]

			if new then
				if not new.loot then new.loot = {} end
				install_loot_table(original.loot, new.loot, 1 + (level - 1) * settings.startup['enemy-fixes-enemy-race-manager-loot-copy-scale'].value)
			end
		end
	end
end
