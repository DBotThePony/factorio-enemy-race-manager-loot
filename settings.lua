
-- Copyright (C) 2022 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

data:extend({
	{
		type = 'bool-setting',
		name = 'enemy-fixes-alien-ore-size',
		setting_type = 'startup',
		default_value = true,
	},
	{
		type = 'double-setting',
		name = 'enemy-fixes-enemy-race-manager-loot-copy-scale',
		localised_description = {'mod-setting-description.enemy-fixes-enemy-race-manager-loot-copy-scale'},
		setting_type = 'startup',
		default_value = 0.2,
		minimum_value = 0.01,
	},
	{
		type = 'double-setting',
		name = 'enemy-fixes-enemy-race-manager-schall-loot-scale',
		localised_description = {'mod-setting-description.enemy-fixes-enemy-race-manager-schall-loot-scale'},
		setting_type = 'startup',
		default_value = 1,
		minimum_value = 0.01,
	},
	{
		type = 'double-setting',
		name = 'enemy-fixes-enemy-race-manager-schall-loot-scale-turret',
		localised_description = {'mod-setting-description.enemy-fixes-enemy-race-manager-schall-loot-scale-turret'},
		setting_type = 'startup',
		default_value = 2,
		minimum_value = 0.01,
	},
	{
		type = 'double-setting',
		name = 'enemy-fixes-enemy-race-manager-schall-loot-scale-spawner',
		localised_description = {'mod-setting-description.enemy-fixes-enemy-race-manager-schall-loot-scale-spawner'},
		setting_type = 'startup',
		default_value = 4,
		minimum_value = 0.01,
	},
})
